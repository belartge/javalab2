import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

import mrth.try0.Parser;
import mrth.try0.errors.LabCalculationException;
import mrth.try0.errors.LabException;
import mrth.try0.util.Calculator;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class CalculatorTest {

   private double precision = 0.00001;

   @Test
   public void arithmeticBinaryOperationsTest() {
      List<Double> numbers = new ArrayList<>();
      numbers.add(3.0);
      numbers.add(4.0);

      try {
         assertEquals(7.0, Calculator.calculate("+", numbers), 0.0);
         assertEquals(-1.0, Calculator.calculate("-", numbers), 0.0);
         assertEquals(12.0, Calculator.calculate("*", numbers), 0.0);
         assertEquals(0.75, Calculator.calculate("/", numbers), 0.0);
      } catch (LabCalculationException aLabCalculationException) {
         assertThat(aLabCalculationException.getMessage(), is("Недостаточное количество операндов для операции +"));
      }
   }

   @Test
   public void arithmeticBinaryOperationsInsufficientOperandsTest() {
      List<Double> numbers = new ArrayList<>();
      numbers.add(3.0);
      try {
         // will fail here 'cause there is only 1 element in numbers (2 required)
         assertEquals(7.0, Calculator.calculate("+", numbers), 0.0);
      } catch (LabCalculationException aLabCalculationException) {
         assertThat(aLabCalculationException.getMessage(), is("Недостаточное количество операндов для операции +"));
      }
   }

   @Test
   public void parsingTest_0() {
      Parser parser = new Parser();
      try {
         String expression = "2+2";
         Double expected = 4.0;
         assertEquals(expected, parser.generateOperand(expression).getValue(), precision);
      } catch (LabException ex) {
         ex.printStackTrace();
      }
   }

   @Test
   public void parsingTest_1() {
      Parser parser = new Parser();
      try {
         String expression = "2+2*2";
         double expected = 6.0;

         assertEquals(expected, parser.generateOperand(expression).getValue(), precision);
      } catch (LabException ex) {
         ex.printStackTrace();
      }
   }

   @Test
   public void parsingTest_2() {
      Parser parser = new Parser();
      try {
         String expression = "(3 + 4) / (10 - 3)";
         double expected = 1.0;
         assertEquals(expected, parser.generateOperand(expression).getValue(), precision);
      } catch (LabException ex) {
         ex.printStackTrace();
      }
   }

   @Test
   public void parsingTest_3() {
      Parser parser = new Parser();
      try {
         String expression = "cos(4-2*2) * 2 / cos(0) - 1";
         double expected = 1.0;
         assertEquals(expected, parser.generateOperand(expression).getValue(), precision);
      } catch (LabException ex) {
         ex.printStackTrace();
      }
   }

   @Test
   public void parsingTest_4() {
      Parser parser = new Parser();
      try {
         String expression = "a * 2 / cos(0) - 1";
         parser.variables.putVariable("a", 4.0);
         double expected = 7.0;
         assertEquals(expected, parser.generateOperand(expression).getValue(), precision);
      } catch (LabException ex) {
         ex.printStackTrace();
      }
   }

   @Test
   public void parsingExceptionTest() {
      Parser parser = new Parser();
      try {
         String expression = "co(4-2*2) * 2 / cos(0) - 1";
         double expected = 1.0;
         assertEquals(expected, parser.generateOperand(expression).getValue(), precision);
      } catch (LabException ex) {
         assertThat(ex.getMessage(), is("Выражение {co(4} задано некорректно"));
      }
   }

   @Test
   public void parsingImbalancedExceptionTest() {
      Parser parser = new Parser();
      try {
         String expression = "(4-2*2))";
         double expected = 0.0;
         assertEquals(expected, parser.generateOperand(expression).getValue(), precision);
      } catch (LabException ex) {
         assertThat(ex.getMessage(), is("Выражение {(4-2*2))} задано некорректно"));
      }
   }
}
