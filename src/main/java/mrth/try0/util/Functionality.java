package mrth.try0.util;

import java.util.HashMap;
import java.util.function.Function;

public class Functionality {

   private HashMap<String, Function<Double, Double>> operators;

   public Functionality() {
      operators = new HashMap<>();

      operators.put("sin", Math::sin);
      operators.put("cos", Math::cos);
      operators.put("tan", Math::tan);
      operators.put("abs", Math::abs);
   }

   public Function<Double, Double> getOperator(String operation) {
      return operators.get(operation);
   }

   public boolean contains(String operation) {
      return operators.keySet().contains(operation);
   }
}
