package mrth.try0.util;

import mrth.try0.errors.InsufficientOperandsException;
import mrth.try0.errors.LabCalculationException;
import mrth.try0.errors.LabException;

import java.util.List;

public class Calculator {

   public static Double calculate(String operation, List<Double> operands) throws LabCalculationException {

      if (isBinaryOperation(operation)) {
         if (operands.size() != 2) {
            throw new InsufficientOperandsException("Недостаточное количество операндов для операции " + operation);
         }

         switch (operation) {
            case "+": return add(operands.get(0), operands.get(1));
            case "-": return sub(operands.get(0), operands.get(1));
            case "*": return mul(operands.get(0), operands.get(1));
            case "/": return div(operands.get(0), operands.get(1));
         }
      }

      return 0.0;
   }

   private static boolean isBinaryOperation(String operation) {
      return operation.equals("+") ||
              operation.equals("-") ||
              operation.equals("*") ||
              operation.equals("/");
   }

   private static Double add(Double firstNumber, Double secondNumber) {
      return firstNumber + secondNumber;
   }

   private static Double mul(Double firstNumber, Double secondNumber) {
      return firstNumber * secondNumber;
   }

   private static Double sub(Double firstNumber, Double secondNumber) {
      return firstNumber - secondNumber;
   }

   private static Double div(Double firstNumber, Double secondNumber) {
      return firstNumber / secondNumber;
   }
}
