package mrth.try0.util;

import java.util.HashMap;
import java.util.Scanner;

public class Variables {

   private HashMap<String, Double> variables = new HashMap<>();
   private Scanner scanner = new Scanner(System.in);

   public Variables() {

   }

   public boolean isVariable(String operand) {
      return variables.keySet().contains(operand);
   }

   public void putVariable(String variable) {
      if (!variables.keySet().contains(variable)) {
         variables.put(variable, Double.NaN);
      }
   }

   public void putVariable(String variable, Double value) {
      if (!variables.keySet().contains(variable)) {
         variables.put(variable, value);
      }
   }

   public boolean areVariablesReady() {
      for (Double number : variables.values()) {
         if (number.isNaN()) {
            return false;
         }
      }
      return true;
   }

   public void prepareVariables() {
      for (String key : variables.keySet()) {
         askForVariable(key);
      }
   }

   public Double getValue(String variable) {
      if (variables.get(variable).isNaN()) {
         askForVariable(variable);
      }
      return variables.get(variable);
   }

   private void askForVariable(String varName) {
      System.out.print("Input " + varName + ": ");
      variables.put(varName, scanner.nextDouble());
   }
}
