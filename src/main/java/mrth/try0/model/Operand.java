package mrth.try0.model;

import mrth.try0.errors.LabCalculationException;

public interface Operand<T> {
   T getValue() throws LabCalculationException;
}
