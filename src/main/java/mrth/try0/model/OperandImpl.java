package mrth.try0.model;

import mrth.try0.errors.LabCalculationException;
import mrth.try0.util.Variables;

public class OperandImpl implements Operand<Double> {

   private Operator<Double> operator;
   private Double number;
   private String variable;

   private Variables variables;

   public OperandImpl(Operator operation) {
      operator = operation;
   }

   public OperandImpl(Double number) {
      this.number = number;
   }

   public OperandImpl(String variable) {
      if (convertableToDouble(variable)) {
         this.number = Double.parseDouble(variable);
      } else {
         this.variable = variable;
      }
   }

   private boolean convertableToDouble(String expression) {
      for (char c : expression.toCharArray()) {
         if (!(c >= '0' && c <= '9' || c == '.')) {
            return false;
         }
      }
      return true;
   }

   @Override
   public Double getValue() throws LabCalculationException {
      if (variable != null) {
         return variables.getValue(variable);
      }
      if (number == null) {
         return operator.calculate();
      }
      return number;
   }

   private boolean isVariable() {
      return variable != null && !variable.equals("");
   }

   public void putVariables(Variables variables) {
      this.variables = variables;
   }
}
