package mrth.try0.model;

import mrth.try0.errors.LabCalculationException;

public interface Operator<T> {
   T calculate() throws LabCalculationException;
}
