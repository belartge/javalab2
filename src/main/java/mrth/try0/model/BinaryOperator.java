package mrth.try0.model;

import mrth.try0.errors.LabCalculationException;
import mrth.try0.util.Calculator;

import java.util.ArrayList;
import java.util.List;

public class BinaryOperator implements Operator<Double> {

   private Operand<Double> left;
   private Operand<Double> right;
   private String operation;

   public BinaryOperator(String operation, Operand left, Operand right) {
      this.left = left;
      this.right = right;
      this.operation = operation;
   }

   @Override
   public Double calculate() throws LabCalculationException {
      if (operation == null || operation.equals("")) {
         return left.getValue();
      }

      List<Double> operands = new ArrayList<>();
      operands.add(left.getValue());
      operands.add(right.getValue());

      return Calculator.calculate(operation, operands);
   }

}
