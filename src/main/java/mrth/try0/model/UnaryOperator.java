package mrth.try0.model;

import mrth.try0.errors.LabCalculationException;
import mrth.try0.util.Functionality;


public class UnaryOperator implements Operator<Double> {

   public Functionality functionality;
   private Operand<Double> operand;
   private String operation;

   public UnaryOperator(String operation, Operand<Double> operand) {
      this.operand = operand;
      this.operation = operation;
   }

   @Override
   public Double calculate() throws LabCalculationException {
      if (operation == null || operation.equals("")) {
         return operand.getValue();
      }

      return functionality.getOperator(operation).apply(operand.getValue());
   }


}
