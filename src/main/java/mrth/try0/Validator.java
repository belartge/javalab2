package mrth.try0;

import mrth.try0.errors.ImbalancedExpressionException;
import mrth.try0.errors.InvalidExpressionException;

class Validator {

   protected boolean isDigit(char character) {
      return (character >= '0') && (character <= '9');
   }

   protected boolean isLetter(char character) {
      return (character >= 'a') && (character <= 'z') || (character >= 'A') && (character <= 'Z');
   }

   protected boolean isArithmeticSign(char character) {
      return (character == '+' || character == '-' || character == '*' || character == '/');
   }

   protected short isParenthesis(char character) {
      // 0 - not a parenthesis
      // 1 - opening parenthesis
      // -1 - closing parenthesis
      if (character == '(') {
         return 1;
      }
      if (character == ')') {
         return -1;
      }
      return 0;
   }

   private boolean isExpressionBalanced(String expression) {
      int balance = 0;
      for (char c : expression.toCharArray()) {
         balance += isParenthesis(c);
      }
      return balance == 0;
   }

   private boolean isExpressionValid(String expression) {
      boolean flag = true;
      for (char c : expression.toCharArray()) {
         flag = flag && (isDigit(c) || isLetter(c) || isParenthesis(c) != 0 ||
                 isArithmeticSign(c) || c == ' ');
      }
      return flag;
   }

   void lookThrough(String expression) throws InvalidExpressionException {
      if (!isExpressionBalanced(expression)) {
         throw new InvalidExpressionException( expression);
      }

      if (!isExpressionBalanced(expression)) {
         throw new ImbalancedExpressionException(expression);
      }
   }

}
