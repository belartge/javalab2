package mrth.try0;

import mrth.try0.errors.InvalidExpressionException;
import mrth.try0.model.BinaryOperator;
import mrth.try0.model.OperandImpl;
import mrth.try0.model.UnaryOperator;
import mrth.try0.util.Functionality;
import mrth.try0.util.Variables;

import java.util.*;

public class Parser {

   private Validator validator;
   private static final Functionality functionalSuite = new Functionality();
   public final Variables variables = new Variables();

   public Parser() {
      validator = new Validator();
   }

   public OperandImpl generateOperand(String expression) throws InvalidExpressionException {
      validator.lookThrough(expression);
      expression = prepare(expression);

      if (isOnlyANumber(expression)) {
        return createNumOperand(expression);
      }

      if (isOnlyAVariable(expression)) {
         return createVarOperand(expression);
      }

      int indexOfFunctionHeader = isOnlyAFunction(expression);
      if(indexOfFunctionHeader > 0) {
         UnaryOperator operator = new UnaryOperator(expression.substring(0, indexOfFunctionHeader),
                 generateOperand(expression.substring(indexOfFunctionHeader+1, expression.length() - 1)));
         operator.functionality = functionalSuite;
         return new OperandImpl(operator);
      } else if (indexOfFunctionHeader == -2) {
         int indexOfSign = findCloseParenthesis(expression, expression.indexOf('(')) +1;
         return buildOperand(expression, indexOfSign);
      }

//      int balance = 0;
//      boolean balanceWasChanged = false;
//
      int startParenthesisIndex = containsParenthesis(expression);
      if (startParenthesisIndex == 0) {
         int endParenthesisIndex = findCloseParenthesis(expression, 0);
         if (endParenthesisIndex == expression.length() - 1) {
            return generateOperand(expression.substring(1, endParenthesisIndex));
         }

         // после скобки продолжение выражения
         return buildOperand(expression, endParenthesisIndex + 1);
      } else if (startParenthesisIndex > 0 && !isParenthesisPartOfFunction(expression, startParenthesisIndex)) {
         return buildOperand(expression, startParenthesisIndex - 1);
      }

      Character[] lowPrioritySigns = {'+', '-'};
      Character[] midPrioritySigns = {'*', '/'};

      int index = findSigns(expression, lowPrioritySigns);

      if (possibleToConstructOperand(expression, index)) {
         // найден знак арифметической операции низкого приоритета
         return buildOperand(expression, index);
      }

      index = findSigns(expression, midPrioritySigns);

      if (possibleToConstructOperand(expression, index)) {
         // найден знак арифметической операции среднего приоритета
         return buildOperand(expression, index);
      }
      return new OperandImpl(0.0);
   }

   private OperandImpl createVarOperand(String expression) {
      variables.putVariable(expression);
      OperandImpl operand = new OperandImpl(expression);
      operand.putVariables(variables);
      return operand;
   }

   private OperandImpl createNumOperand(String expression) {
      OperandImpl operand = new OperandImpl(expression);
      operand.putVariables(variables);
      return operand;
   }

   private boolean isParenthesisPartOfFunction(String expression, int index) {
      if (index == 0) {
         return false;
      }
      return validator.isLetter(expression.charAt(index - 1));
   }

   private boolean possibleToConstructOperand(String expression, int index) {
      return index < expression.length();
   }

   private String prepare(String expression) {
      if (expression.charAt(0) == '-') {
         return "0" + expression;
      }
      expression = expression.replaceAll(" ", "");
      return expression;
   }

   private int findSigns(String expression, Character[] signs) {
      int index = 0;
      List<Character> list = Arrays.asList(signs);

      while (index < expression.length() && !list.contains(expression.charAt(index))) {
         index++;
      }
      return index;
   }

   private OperandImpl buildOperand(String expression, int index) throws InvalidExpressionException {
      OperandImpl leftPart = generateOperand(expression.substring(0, index));
      OperandImpl rightPart = generateOperand(expression.substring(index + 1));

      return new OperandImpl(new BinaryOperator(String.valueOf(expression.charAt(index)), leftPart, rightPart));
   }

   private boolean isOnlyANumber(String expression) {
      for (char character : expression.toCharArray()) {
         if (character < '0' || character > '9') {
            return false;
         }
      }
      return true;
   }

   private boolean isOnlyAVariable(String expression) {
      for (char character : expression.toCharArray()) {
         if (!validator.isLetter(character)) {
            return false;
         }
      }
      return true;
   }

   private int isOnlyAFunction(String expression) {
      // проверяет, является ли выражение только вызовом некоторой функции
      // возвращает > 0, если это так. число - конец заголовка функции
      // иначе возвращает -1
      if (validator.isLetter(expression.charAt(0))) {
         int index = 0;
         while(validator.isLetter(expression.charAt(index)) || validator.isParenthesis(expression.charAt(index))!= 0) {
            index++;
         }

         int endOfFunction = findCloseParenthesis(expression, index);

         index--; //скобки в цикле выше также подсчитывались

         boolean leftFlag = functionalSuite.contains(expression.substring(0, index));
         boolean rightFlag = endOfFunction == expression.length() - 1;
         if (leftFlag) {
            if (rightFlag) {
               return index;
            } else {
               return -2;
            }
         } else {
            //not a function
            return -3;
         }
      }
      return -1;
   }

   private int containsParenthesis(String expression) {
      return expression.indexOf('(');
   }

   private int findCloseParenthesis(String expression, int indexOfOpening) {
      int balance = 1;
      for (int index = indexOfOpening + 1; index < expression.length(); index++) {
         balance += validator.isParenthesis(expression.charAt(index));
         if (balance == 0) {
            return index;
         }
      }
      return -1;
   }
}
