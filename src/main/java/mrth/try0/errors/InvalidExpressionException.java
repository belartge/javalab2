package mrth.try0.errors;

public class InvalidExpressionException extends LabParsingException {
   public InvalidExpressionException(String message) {
      super("Выражение {" + message + "} задано некорректно");
   }
}
