package mrth.try0.errors;

public class LabParsingException extends LabException {
   public LabParsingException(String message) {
      super(message);
   }
}
