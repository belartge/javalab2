package mrth.try0.errors;

public class LabCalculationException extends LabException {
   public LabCalculationException(String message) {
      super(message);
   }
}
