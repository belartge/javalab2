package mrth.try0.errors;

public class InsufficientOperandsException extends LabCalculationException {
   public InsufficientOperandsException(String message) {
      super(message);
   }
}
