package mrth.try0.errors;

public class LabException extends Exception {
   public LabException(String message) {
      super(message);
   }
}
