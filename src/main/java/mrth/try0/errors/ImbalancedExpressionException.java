package mrth.try0.errors;

public class ImbalancedExpressionException extends InvalidExpressionException {
   public ImbalancedExpressionException(String message) {
      super("В выражении {" + message + "} не сбалансированы скобки");
   }
}
