package mrth.try0;

import mrth.try0.errors.LabException;
import mrth.try0.model.BinaryOperator;
import mrth.try0.model.OperandImpl;
import mrth.try0.model.UnaryOperator;

public class Program {
   public static void main(String[] args) {
      Parser parser = new Parser();
      try {
         System.out.println(parser.generateOperand("2+2*2").getValue());
         System.out.println(parser.generateOperand("(3/2)").getValue());
         System.out.println(parser.generateOperand("(2+2)*(1+1)").getValue());
         System.out.println(parser.generateOperand("2*(1+1)").getValue());
         System.out.println(parser.generateOperand("cos(4-2*2)*2/cos(0)-1").getValue());
         System.out.println(parser.generateOperand("x+x/x").getValue());

//         System.out.println(operand);
//         System.out.println(operand.getValue());
      } catch (LabException ex) {
         ex.printStackTrace();
      }

      //TODO: 3.1. если не все операнды выражания не заданы, запросить значения
   }

//   static String prepare(String e) {
//      e = e.replaceAll("-", "+(-1)*");
//      if (e.charAt(0) == '+' || e.charAt(0) == '-') {
//         e = "0" + e;
//      }
//      return e;
//   }
}
